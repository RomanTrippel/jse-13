package ru.trippel.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trippel.tm.api.context.IServiceLocator;
import ru.trippel.tm.api.endpoint.IUserEndpoint;
import ru.trippel.tm.entity.Session;
import ru.trippel.tm.entity.User;
import ru.trippel.tm.enumeration.TypeRole;
import ru.trippel.tm.util.PasswordHashUtil;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@NoArgsConstructor
@WebService(endpointInterface = "ru.trippel.tm.api.endpoint.IUserEndpoint")
public class UserEndpoint implements IUserEndpoint {

    private IServiceLocator serviceLocator;

    public UserEndpoint(@NotNull final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Nullable
    @Override
    @WebMethod
    public User createUser(
                           @WebParam(name = "loginName") @NotNull final String loginName,
                           @WebParam(name = "password") @NotNull final String password
    ) throws Exception {
        @NotNull final User user = new User();
        user.setLoginName(loginName);
        user.setPassword(PasswordHashUtil.getHash(password));
        return serviceLocator.getUserService().persist(user);
    }

    @Override
    @WebMethod
    public void removeAllUsers(
            @WebParam(name = "token") @NotNull final String token
    ) throws Exception {
        @Nullable final Session session = serviceLocator.getSessionService().getSession(token);
        serviceLocator.getSessionService().validate(session, TypeRole.ADMIN);
        serviceLocator.getUserService().removeAll();
    }

    @Nullable
    @Override
    @WebMethod
    public List<User> findAllUser(
            @WebParam(name = "token") @NotNull final String token
    ) throws Exception {
        @Nullable final Session session = serviceLocator.getSessionService().getSession(token);
        serviceLocator.getSessionService().validate(session, TypeRole.ADMIN);
        return serviceLocator.getUserService().findAll();
    }

    @Nullable
    @Override
    @WebMethod
    public User findOneUser(
            @WebParam(name = "token") @NotNull final String token
    ) throws Exception {
        @Nullable final Session session = serviceLocator.getSessionService().getSession(token);
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getUserService().findOne(session.getUserId());
    }

    @Nullable
    @Override
    @WebMethod
    public User findByLoginNameUser(
            @WebParam(name = "token") @NotNull final String token,
            @WebParam(name = "loginName") @NotNull final String loginName
    ) throws Exception {
        @Nullable final Session session = serviceLocator.getSessionService().getSession(token);
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getUserService().findByLoginName(loginName);
    }

    @Nullable
    @Override
    @WebMethod
    public User updateUser(
            @WebParam(name = "token") @NotNull final String token,
            @WebParam(name = "user") @NotNull final User user
    ) throws Exception {
        @Nullable final Session session = serviceLocator.getSessionService().getSession(token);
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getUserService().merge(user);
    }

    @Override
    @WebMethod
    public void removeUser(
            @WebParam(name = "token") @NotNull final String token,
            @WebParam(name = "userId") @NotNull final String userId
    ) throws Exception {
        @Nullable final Session session = serviceLocator.getSessionService().getSession(token);
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService().remove(userId);
    }

    @Override
    @WebMethod
    public boolean checkLoginUser(
            @WebParam(name = "loginName") @NotNull final String loginName
    ) throws Exception {
        return serviceLocator.getUserService().checkLogin(loginName);
    }

}
