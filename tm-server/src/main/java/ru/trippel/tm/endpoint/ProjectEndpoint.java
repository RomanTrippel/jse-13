package ru.trippel.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trippel.tm.api.context.IServiceLocator;
import ru.trippel.tm.api.endpoint.IProjectEndpoint;
import ru.trippel.tm.entity.Project;
import ru.trippel.tm.entity.Session;
import ru.trippel.tm.entity.User;
import ru.trippel.tm.enumeration.SortingMethod;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@NoArgsConstructor
@WebService(endpointInterface = "ru.trippel.tm.api.endpoint.IProjectEndpoint")
public class ProjectEndpoint implements IProjectEndpoint {

    private IServiceLocator serviceLocator;

    public ProjectEndpoint(@NotNull final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Nullable
    @Override
    @WebMethod
    public Project createProject(@WebParam(name = "token") @NotNull final String token,
                                 @WebParam(name = "projectName") @NotNull final String projectName
    ) throws Exception {
        @Nullable final Session session = serviceLocator.getSessionService().getSession(token);
        serviceLocator.getSessionService().validate(session);
        @NotNull final Project project = new Project();
        project.setName(projectName);
        project.setUserId(session.getUserId());
        return serviceLocator.getProjectService().persist(project);
    }

    @Nullable
    @Override
    @WebMethod
    public List<Project> findAllProjectsBySort(
            @WebParam(name = "token") @NotNull final String token
    ) throws Exception {
        @Nullable final Session session = serviceLocator.getSessionService().getSession(token);
        serviceLocator.getSessionService().validate(session);
        @Nullable final String userId = session.getUserId();
        @Nullable final User user = serviceLocator.getUserService().findOne(userId);
        if (userId == null || user == null) return null;
        @NotNull final SortingMethod sortingMethod = user.getProjectSortingMethod();
        return serviceLocator.getProjectService().findAllByComparator(session.getUserId(), sortingMethod);
    }

    @Override
    @WebMethod
    public void clearProjects(
            @WebParam(name = "token") @NotNull final String token
    ) throws Exception {
        @Nullable final Session session = serviceLocator.getSessionService().getSession(token);
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectService().clear(session.getUserId());
    }

    @Override
    @WebMethod
    public void removeAllProjects(
            @WebParam(name = "token") @NotNull final String token
    ) throws Exception {
        @Nullable final Session session = serviceLocator.getSessionService().getSession(token);
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectService().removeAll();
    }

    @NotNull
    @Override
    @WebMethod
    public final List<Project> findAllProject(
            @WebParam(name = "token") @NotNull final String token
    ) throws Exception {
        @Nullable final Session session = serviceLocator.getSessionService().getSession(token);
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().findAll();
    }

    @Nullable
    @Override
    @WebMethod
    public Project findOneProject(
            @WebParam(name = "token") @NotNull final String token,
            @WebParam(name = "projectId") @NotNull final String projectId
    ) throws Exception {
        @Nullable final Session session = serviceLocator.getSessionService().getSession(token);
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().findOne(projectId);
    }

    @Nullable
    @Override
    @WebMethod
    public Project updateProject(
            @WebParam(name = "token") @NotNull final String token,
            @WebParam(name = "project") @NotNull final Project project
    ) throws Exception {
        @Nullable final Session session = serviceLocator.getSessionService().getSession(token);
        serviceLocator.getSessionService().validate(session);
        @NotNull final Project projectTemp = project;
        projectTemp.setUserId(session.getUserId());
        return serviceLocator.getProjectService().merge(projectTemp);
    }

    @Override
    @WebMethod
    public void removeProject(
            @WebParam(name = "token") @NotNull final String token,
            @WebParam(name = "projectId") @NotNull final String projectId
    ) throws Exception {
        @Nullable final Session session = serviceLocator.getSessionService().getSession(token);
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectService().remove(projectId);
    }

    @Nullable
    @Override
    @WebMethod
    public List<Project> findAllProjectsByUserId(
            @WebParam(name = "token") @NotNull final String token
    ) throws Exception {
        @Nullable final Session session = serviceLocator.getSessionService().getSession(token);
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().findAllById(session.getUserId());
    }

    @Nullable
    @Override
    @WebMethod
    public List<Project> findProjectByPart(
            @WebParam(name = "token") @NotNull final String token,
            @WebParam(name = "searchPhrase") @NotNull final String searchText
    ) throws Exception {
        @Nullable final Session session = serviceLocator.getSessionService().getSession(token);
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().findByPart(session.getUserId(), searchText);
    }

}
