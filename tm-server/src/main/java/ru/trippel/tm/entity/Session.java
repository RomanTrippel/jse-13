package ru.trippel.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trippel.tm.enumeration.TypeRole;

@Getter
@Setter
@NoArgsConstructor
public class Session extends AbstractEntity{

    @Nullable
    String userId = "";

    @Nullable
    String signature = "";

    @NotNull
    private Long createDate = System.currentTimeMillis();

    @Nullable
    TypeRole role = TypeRole.USER;

}
