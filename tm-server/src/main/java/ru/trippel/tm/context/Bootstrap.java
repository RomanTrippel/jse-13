package ru.trippel.tm.context;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.apache.ibatis.datasource.pooled.PooledDataSource;
import org.apache.ibatis.mapping.Environment;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.apache.ibatis.transaction.TransactionFactory;
import org.apache.ibatis.transaction.jdbc.JdbcTransactionFactory;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trippel.tm.api.context.IServiceLocator;
import ru.trippel.tm.api.endpoint.*;
import ru.trippel.tm.api.repository.IProjectRepository;
import ru.trippel.tm.api.repository.ISessionRepository;
import ru.trippel.tm.api.repository.ITaskRepository;
import ru.trippel.tm.api.repository.IUserRepository;
import ru.trippel.tm.api.service.*;
import ru.trippel.tm.endpoint.*;
import ru.trippel.tm.entity.User;
import ru.trippel.tm.enumeration.TypeRole;
import ru.trippel.tm.service.*;
import ru.trippel.tm.util.PasswordHashUtil;
import ru.trippel.tm.util.PropertyUtil;

import javax.sql.DataSource;
import javax.xml.ws.Endpoint;
import java.sql.SQLException;

@Getter
@NoArgsConstructor
public final class Bootstrap implements IServiceLocator {

    @NotNull
    private final IProjectService projectService = new ProjectService(this);

    @NotNull
    private final ITaskService taskService = new TaskService(this);

    @NotNull
    private final IUserService userService = new UserService(this);

    @NotNull
    private final ISessionService sessionService = new SessionService(this);

    @NotNull
    public final ISubjectArea subjectAreaService = new SubjectAreaService();

    @NotNull
    private final IDataService dataService = new DataService(this);

    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpoint(this);

    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpoint(this);

    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpoint(this);

    @NotNull
    private final ISessionEndpoint sessionEndpoint = new SessionEndpoint(this);

    @NotNull
    private final IDataEndpoint dataEndpoint = new DataEndpoint(this);

    @Nullable
    private SqlSessionFactory sqlSessionFactory;

    private void initDefaultUser() throws SQLException {
        @NotNull final User admin = new User();
        admin.setLoginName("admin");
        admin.setPassword(PasswordHashUtil.getHash("admin"));
        admin.setRole(TypeRole.ADMIN);
        userService.persist(admin);
        @NotNull final User user = new User();
        user.setLoginName("user");
        user.setPassword(PasswordHashUtil.getHash("user"));
        userService.persist(user);
    }

    private SqlSessionFactory initSqlSessionFactory() throws Exception {
        @NotNull final String login = PropertyUtil.dbLogin();
        @NotNull final String password = PropertyUtil.dbPassword();
        @NotNull final String host = PropertyUtil.dbHost();
        @NotNull final String driver = PropertyUtil.dbDriver();
        @NotNull final DataSource dataSource = new PooledDataSource(driver, host, login, password);
        @NotNull final TransactionFactory transactionFactory = new JdbcTransactionFactory();
        @NotNull final Environment environment = new Environment("development", transactionFactory, dataSource);
        @NotNull final Configuration configuration = new Configuration(environment);
        configuration.addMapper(IProjectRepository.class);
        configuration.addMapper(ITaskRepository.class);
        configuration.addMapper(IUserRepository.class);
        configuration.addMapper(ISessionRepository.class);
        return new SqlSessionFactoryBuilder().build(configuration);
    }

    public void start() {
        try {
            initDefaultUser();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        Endpoint.publish("http://localhost:8080/ProjectEndpoint?wsdl", projectEndpoint);
        Endpoint.publish("http://localhost:8080/TaskEndpoint?wsdl", taskEndpoint);
        Endpoint.publish("http://localhost:8080/UserEndpoint?wsdl", userEndpoint);
        Endpoint.publish("http://localhost:8080/SessionEndpoint?wsdl", sessionEndpoint);
        Endpoint.publish("http://localhost:8080/DataEndpoint?wsdl", dataEndpoint);
        try {
            sqlSessionFactory = initSqlSessionFactory();
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("Sm-server started successfully.");
    }

}