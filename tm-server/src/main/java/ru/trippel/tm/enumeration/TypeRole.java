package ru.trippel.tm.enumeration;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;

public enum  TypeRole {

    @NotNull
    ADMIN("Admin"),
    @NotNull
    USER("User");

    @Getter
    @NotNull
    private String displayName = "";

    TypeRole(@NotNull final String displayName) {
        this.displayName = displayName;
    }

}
