package ru.trippel.tm.service;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trippel.tm.api.repository.ITaskRepository;
import ru.trippel.tm.api.service.ITaskService;
import ru.trippel.tm.context.Bootstrap;
import ru.trippel.tm.entity.Task;
import ru.trippel.tm.enumeration.SortingMethod;
import ru.trippel.tm.util.ComparatorUtil;

import java.sql.SQLException;
import java.util.List;

public final class TaskService implements ITaskService {

    @NotNull
    private final Bootstrap bootstrap;

    public TaskService(Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    @Nullable
    @Override
    public Task persist(@Nullable Task task) throws SQLException {
        if (task == null) return null;
        @Nullable Task result = task;
        if (bootstrap.getSqlSessionFactory() == null) return null;
        final @NotNull SqlSession sqlSession = bootstrap.getSqlSessionFactory().openSession();
        final @NotNull ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
        try {
            taskRepository.persist(task);
            sqlSession.commit();
        } catch (SQLException e) {
            sqlSession.rollback();
            result = null;
        }
        finally {
            sqlSession.close();
        }
        return result;
    }

    @Nullable
    @Override
    public List<Task> findAllByComparator(@Nullable final String userId, @NotNull final SortingMethod sortingMethod)
            throws SQLException {
        if (userId == null) return null;
        if (userId.isEmpty()) return null;
        @Nullable List<Task> result = null;
        if (bootstrap.getSqlSessionFactory() == null) return null;
        final @NotNull SqlSession sqlSession = bootstrap.getSqlSessionFactory().openSession();
        final @NotNull ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
        try {
            @Nullable String sort = ComparatorUtil.getComparator(sortingMethod);
            result = taskRepository.findAllByComparator(userId, sort);
            sqlSession.commit();
        } catch (SQLException e) {
            sqlSession.rollback();
        }
        finally {
            sqlSession.close();
        }
        return result;
    }

    @Nullable
    @Override
    public List<Task> findAll() throws SQLException {
        @Nullable List<Task> result = null;
        if (bootstrap.getSqlSessionFactory() == null) return null;
        final @NotNull SqlSession sqlSession = bootstrap.getSqlSessionFactory().openSession();
        final @NotNull ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
        try {
            result = taskRepository.findAll();
            sqlSession.commit();
        } catch (SQLException e) {
            sqlSession.rollback();
        }
        finally {
            sqlSession.close();
        }
        return result;
    }

    @Nullable
    @Override
    public List<Task> findAllById(@Nullable final String userId) throws SQLException {
        if (userId == null || userId.isEmpty()) return null;
        @Nullable List<Task> result = null;
        if (bootstrap.getSqlSessionFactory() == null) return null;
        final @NotNull SqlSession sqlSession = bootstrap.getSqlSessionFactory().openSession();
        final @NotNull ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
        try {
            result = taskRepository.findAllById(userId);
            sqlSession.commit();
        } catch (SQLException e) {
            sqlSession.rollback();
        }
        finally {
            sqlSession.close();
        }
        return result;
    }

    @Nullable
    @Override
    public Task findOne(@Nullable String id) throws SQLException {
        if (id == null || id.isEmpty()) return null;
        @Nullable Task result = null;
        if (bootstrap.getSqlSessionFactory() == null) return null;
        final @NotNull SqlSession sqlSession = bootstrap.getSqlSessionFactory().openSession();
        final @NotNull ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
        try {
            result = taskRepository.findOne(id);
            sqlSession.commit();
        } catch (SQLException e) {
            sqlSession.rollback();
        }
        finally {
            sqlSession.close();
        }
        return result;
    }

    @Nullable
    @Override
    public List<Task> findByPart(@Nullable final String userId, @NotNull final String searchText) throws SQLException {
        if (userId == null || userId.isEmpty()) return null;
        if (searchText == null || searchText.isEmpty()) return null;
        @Nullable List<Task> result = null;
        if (bootstrap.getSqlSessionFactory() == null) return null;
        final @NotNull SqlSession sqlSession = bootstrap.getSqlSessionFactory().openSession();
        final @NotNull ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
        try {
            result = taskRepository.findByPart(userId, searchText);
            sqlSession.commit();
        } catch (SQLException e) {
            sqlSession.rollback();
        }
        finally {
            sqlSession.close();
        }
        return result;
    }

    @Override
    public void clear(@Nullable final String userId) throws SQLException {
        if (userId == null) return;
        if (userId.isEmpty()) return;
        if (bootstrap.getSqlSessionFactory() == null) return;
        final @NotNull SqlSession sqlSession = bootstrap.getSqlSessionFactory().openSession();
        final @NotNull ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
        try {
            taskRepository.clear(userId);
            sqlSession.commit();
        } catch (SQLException e) {
            sqlSession.rollback();
        }
        finally {
            sqlSession.close();
        }
    }

    @Override
    public void removeAll() throws SQLException {
        if (bootstrap.getSqlSessionFactory() == null) return;
        final @NotNull SqlSession sqlSession = bootstrap.getSqlSessionFactory().openSession();
        final @NotNull ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
        try {
            taskRepository.removeAll();
            sqlSession.commit();
        } catch (SQLException e) {
            sqlSession.rollback();
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void remove(@Nullable String id) throws SQLException {
        if (id == null || id.isEmpty()) return;
        if (bootstrap.getSqlSessionFactory() == null) return;
        final @NotNull SqlSession sqlSession = bootstrap.getSqlSessionFactory().openSession();
        final @NotNull ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
        try {
            taskRepository.removeOne(id);
            sqlSession.commit();
        } catch (SQLException e) {
            sqlSession.rollback();
        }
        finally {
            sqlSession.close();
        }
    }

    @Override
    public void removeAllByProjectId(@NotNull final String projectId) throws SQLException {
        if (projectId == null || projectId.isEmpty()) return;
        if (bootstrap.getSqlSessionFactory() == null) return;
        final @NotNull SqlSession sqlSession = bootstrap.getSqlSessionFactory().openSession();
        final @NotNull ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
        try {
            taskRepository.removeByProjectId(projectId);
            sqlSession.commit();
        } catch (SQLException e) {
            sqlSession.rollback();
        }
        finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    public Task merge(@Nullable Task task) throws SQLException {
        if (task == null) return null;
        @Nullable Task result = task;
        if (bootstrap.getSqlSessionFactory() == null) return null;
        final @NotNull SqlSession sqlSession = bootstrap.getSqlSessionFactory().openSession();
        final @NotNull ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
        try {
            taskRepository.merge(task);
            sqlSession.commit();
        } catch (SQLException e) {
            sqlSession.rollback();
            result = null;
        }
        finally {
            sqlSession.close();
        }
        return result;
    }

}
