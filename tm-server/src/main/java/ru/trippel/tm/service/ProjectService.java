package ru.trippel.tm.service;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trippel.tm.api.repository.IProjectRepository;
import ru.trippel.tm.api.service.IProjectService;
import ru.trippel.tm.context.Bootstrap;
import ru.trippel.tm.entity.Project;
import ru.trippel.tm.enumeration.SortingMethod;
import ru.trippel.tm.util.ComparatorUtil;

import java.sql.SQLException;
import java.util.List;

public final class ProjectService implements IProjectService {

    @NotNull
    private final Bootstrap bootstrap;

    public ProjectService(@NotNull final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    @Nullable
    @Override
    public Project persist(@Nullable final Project project) throws SQLException {
        if (project == null) return null;
        @Nullable Project result = project;
        if (bootstrap.getSqlSessionFactory() == null) return null;
        final @NotNull SqlSession sqlSession = bootstrap.getSqlSessionFactory().openSession();
        final @NotNull IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
        try {
            projectRepository.persist(project);
            sqlSession.commit();
        } catch (SQLException e) {
            sqlSession.rollback();
            result = null;
        }
        finally {
         sqlSession.close();
        }
        return result;
    }

    @Nullable
    @Override
    public List<Project> findAllByComparator(@Nullable final String userId, @NotNull final SortingMethod sortingMethod)
            throws SQLException {
        if (userId == null) return null;
        if (userId.isEmpty()) return null;
        @Nullable List<Project> result = null;
        if (bootstrap.getSqlSessionFactory() == null) return null;
        final @NotNull SqlSession sqlSession = bootstrap.getSqlSessionFactory().openSession();
        final @NotNull IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
        try {
            @Nullable String sort = ComparatorUtil.getComparator(sortingMethod);
            result = projectRepository.findAllByComparator(userId, sort);
            sqlSession.commit();
        } catch (SQLException e) {
            sqlSession.rollback();
        }
        finally {
            sqlSession.close();
        }
        return result;
    }

    @Nullable
    @Override
    public List<Project> findAll() throws SQLException {
        @Nullable List<Project> result = null;
        if (bootstrap.getSqlSessionFactory() == null) return null;
        final @NotNull SqlSession sqlSession = bootstrap.getSqlSessionFactory().openSession();
        final @NotNull IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
        try {
            result = projectRepository.findAll();
            sqlSession.commit();
        } catch (SQLException e) {
            sqlSession.rollback();
        }
        finally {
            sqlSession.close();
        }
        return result;
    }

    @Nullable
    @Override
    public List<Project> findAllById(@Nullable final String userId) throws SQLException {
        if (userId == null || userId.isEmpty()) return null;
        @Nullable List<Project> result = null;
        if (bootstrap.getSqlSessionFactory() == null) return null;
        final @NotNull SqlSession sqlSession = bootstrap.getSqlSessionFactory().openSession();
        final @NotNull IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
        try {
            result = projectRepository.findAllById(userId);
            sqlSession.commit();
        } catch (SQLException e) {
            sqlSession.rollback();
        }
        finally {
            sqlSession.close();
        }
        return result;
    }

    @Nullable
    @Override
    public Project findOne(@Nullable final String id) throws SQLException {
        if (id == null || id.isEmpty()) return null;
        @Nullable Project result = null;
        if (bootstrap.getSqlSessionFactory() == null) return null;
        final @NotNull SqlSession sqlSession = bootstrap.getSqlSessionFactory().openSession();
        final @NotNull IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
        try {
            result = projectRepository.findOne(id);
            sqlSession.commit();
        } catch (SQLException e) {
            sqlSession.rollback();
        }
        finally {
            sqlSession.close();
        }
        return result;
    }

    @Nullable
    @Override
    public List<Project> findByPart(@Nullable final String userId, @NotNull final String searchText)
            throws SQLException {
        if (userId == null || userId.isEmpty()) return null;
        if (searchText == null || searchText.isEmpty()) return null;
        @Nullable List<Project> result = null;
        if (bootstrap.getSqlSessionFactory() == null) return null;
        final @NotNull SqlSession sqlSession = bootstrap.getSqlSessionFactory().openSession();
        final @NotNull IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
        try {
            result = projectRepository.findByPart(userId, searchText);
            sqlSession.commit();
        } catch (SQLException e) {
            sqlSession.rollback();
        }
        finally {
            sqlSession.close();
        }
        return result;
    }

    @Override
    public void clear(@Nullable final String userId) throws SQLException {
        if (userId == null) return;
        if (userId.isEmpty()) return;
        if (bootstrap.getSqlSessionFactory() == null) return;
        final @NotNull SqlSession sqlSession = bootstrap.getSqlSessionFactory().openSession();
        final @NotNull IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
        try {
            projectRepository.clear(userId);
            sqlSession.commit();
        } catch (SQLException e) {
            sqlSession.rollback();
        }
        finally {
            sqlSession.close();
        }
    }

    @Override
    public void removeAll() throws SQLException {
        if (bootstrap.getSqlSessionFactory() == null) return;
        final @NotNull SqlSession sqlSession = bootstrap.getSqlSessionFactory().openSession();
        final @NotNull IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
        try {
            projectRepository.removeAll();
            sqlSession.commit();
        } catch (SQLException e) {
            sqlSession.rollback();
        }
        finally {
            sqlSession.close();
        }
    }

    @Override
    public void remove(@Nullable final String id) throws SQLException {
        if (id == null || id.isEmpty()) return;
        if (bootstrap.getSqlSessionFactory() == null) return;
        final @NotNull SqlSession sqlSession = bootstrap.getSqlSessionFactory().openSession();
        final @NotNull IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
        try {
            projectRepository.removeOne(id);
            sqlSession.commit();
        } catch (SQLException e) {
            sqlSession.rollback();
        }
        finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    public Project merge(@Nullable final Project project) throws SQLException {
        if (project == null) return null;
        @Nullable Project result = project;
        if (bootstrap.getSqlSessionFactory() == null) return null;
        final @NotNull SqlSession sqlSession = bootstrap.getSqlSessionFactory().openSession();
        final @NotNull IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
        try {
            projectRepository.merge(project);
            sqlSession.commit();
        } catch (SQLException e) {
            sqlSession.rollback();
            result = null;
        }
        finally {
            sqlSession.close();
        }
        return result;
    }

}
