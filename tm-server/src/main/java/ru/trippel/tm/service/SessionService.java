package ru.trippel.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trippel.tm.api.repository.ISessionRepository;
import ru.trippel.tm.api.service.ISessionService;
import ru.trippel.tm.context.Bootstrap;
import ru.trippel.tm.entity.Session;
import ru.trippel.tm.enumeration.TypeRole;
import ru.trippel.tm.util.EncryptionSessionUtil;
import ru.trippel.tm.util.PropertyUtil;

import java.util.List;

public class SessionService implements ISessionService {

    @NotNull
    private final Bootstrap bootstrap;

    public SessionService(@NotNull final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    @Nullable
    @Override
    public Session persist(@Nullable final Session session) {
        if (session == null) return null;
        @Nullable Session result = session;
        if (bootstrap.getSqlSessionFactory() == null) return null;
        final @NotNull SqlSession sqlSession = bootstrap.getSqlSessionFactory().openSession();
        final @NotNull ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
        try {
            sessionRepository.persist(session);
            sqlSession.commit();
        } catch (Exception e) {
            sqlSession.rollback();
            result = null;
        }
        finally {
            sqlSession.close();
        }
        return result;
    }

    @Override
    public void remove(@Nullable final Session session) {
        if (session == null) return;
        if (bootstrap.getSqlSessionFactory() == null) return;
        final @NotNull SqlSession sqlSession = bootstrap.getSqlSessionFactory().openSession();
        final @NotNull ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
        try {
            sessionRepository.remove(session.getId());
            sqlSession.commit();
        } catch (Exception e) {
            sqlSession.rollback();
        }
        finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    public Session findOne(@Nullable final String id) {
        if (id == null || id.isEmpty()) return null;
        @Nullable Session result = null;
        if (bootstrap.getSqlSessionFactory() == null) return null;
        final @NotNull SqlSession sqlSession = bootstrap.getSqlSessionFactory().openSession();
        final @NotNull ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
        try {
            result = sessionRepository.findOne(id);
            sqlSession.commit();
        } catch (Exception e) {
            sqlSession.rollback();
        }
        finally {
            sqlSession.close();
        }
        return result;
    }


    @Override
    public void validate(@Nullable final Session session, @NotNull TypeRole role) throws Exception {
        if (session == null) return;
        if (!role.equals(session.getRole())) throw new Exception("Not enough rights. You must be an admin.");
        validate(session);
    }

    @Override
    public void validate(@Nullable final Session session) throws Exception {
        if (session == null) throw new Exception("You must log in.");
        if (session.getUserId() == null ||
                session.getSignature() == null ||
                session.getRole() == null) throw new Exception("You must log in.");
        @Nullable final Session sessionInBase = findOne(session.getId());
        if (sessionInBase == null) throw new Exception("No session.");
        @Nullable final String signature = session.getSignature();
        @Nullable final String signatureInBase = sessionInBase.getSignature();
        if (signatureInBase == null) throw new Exception("You must log in.");
        if (!(signatureInBase.equals(signature))) throw new Exception("You must log in.");
        final long passedTime = System.currentTimeMillis() - sessionInBase.getCreateDate();
        if (passedTime > 10*60*1000) {
            remove(session.getId());
            throw new Exception("The session time of 10 minutes has expired. You need to log in.");
        }
    }

    @Nullable
    public String getToken(@Nullable final Session session) throws Exception {
        if (session == null) return null;
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writeValueAsString(session);
        return EncryptionSessionUtil.encrypt(json, PropertyUtil.encryptKey());
    }

    @Nullable
    public Session getSession(@Nullable final String token) throws Exception {
        if (token == null || token.isEmpty()) return null;
        @NotNull String json = EncryptionSessionUtil.decrypt(token, PropertyUtil.encryptKey());
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final Session session = objectMapper.readValue(json, Session.class);
        return session;
    }

    @NotNull
    @Override
    public List<Session> findAll() {
        return null;
    }

    @Nullable
    @Override
    public Session merge(@Nullable final Session session) {
        return null;
    }

    @Override
    public void remove(@Nullable final String id) {
    }

    @Override
    public void removeAll() {
    }

}
