package ru.trippel.tm.service;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trippel.tm.api.repository.IUserRepository;
import ru.trippel.tm.api.service.IUserService;
import ru.trippel.tm.context.Bootstrap;
import ru.trippel.tm.entity.User;

import java.sql.SQLException;
import java.util.List;

public final class UserService implements IUserService {

    @NotNull
    private final Bootstrap bootstrap;

    public UserService(Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    @Nullable
    @Override
    public User persist(@Nullable User user) throws SQLException {
        if (user == null) return null;
        @Nullable User result = user;
        if (bootstrap.getSqlSessionFactory() == null) return null;
        final @NotNull SqlSession sqlSession = bootstrap.getSqlSessionFactory().openSession();
        final @NotNull IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
        try {
            userRepository.persist(user);
            sqlSession.commit();
        } catch (SQLException e) {
            sqlSession.rollback();
            result = null;
        }
        finally {
            sqlSession.close();
        }
        return result;
    }

    @Nullable
    @Override
    public List<User> findAll() throws SQLException {
        @Nullable List<User> result = null;
        if (bootstrap.getSqlSessionFactory() == null) return null;
        final @NotNull SqlSession sqlSession = bootstrap.getSqlSessionFactory().openSession();
        final @NotNull IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
        try {
            result = userRepository.findAll();
            sqlSession.commit();
        } catch (SQLException e) {
            sqlSession.rollback();
        }
        finally {
            sqlSession.close();
        }
        return result;
    }

    @Nullable
    @Override
    public User findOne(@Nullable String id) throws SQLException {
        if (id == null || id.isEmpty()) return null;
        @Nullable User result = null;
        if (bootstrap.getSqlSessionFactory() == null) return null;
        final @NotNull SqlSession sqlSession = bootstrap.getSqlSessionFactory().openSession();
        final @NotNull IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
        try {
            result = userRepository.findOne(id);
            sqlSession.commit();
        } catch (SQLException e) {
            sqlSession.rollback();
        }
        finally {
            sqlSession.close();
        }
        return result;
    }

    @Nullable
    @Override
    public User findByLoginName(@Nullable String loginName) throws SQLException {
        if (loginName == null) return null;
        if (loginName.isEmpty()) return null;
        @Nullable User result = null;
        if (bootstrap.getSqlSessionFactory() == null) return null;
        final @NotNull SqlSession sqlSession = bootstrap.getSqlSessionFactory().openSession();
        final @NotNull IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
        try {
            result = userRepository.findByLoginName(loginName);
            sqlSession.commit();
        } catch (SQLException e) {
            sqlSession.rollback();
        }
        finally {
            sqlSession.close();
        }
        return result;
    }

    @Nullable
    @Override
    public User merge(@Nullable User user) throws SQLException {
        if (user == null) return null;
        @Nullable User result = user;
        if (bootstrap.getSqlSessionFactory() == null) return null;
        final @NotNull SqlSession sqlSession = bootstrap.getSqlSessionFactory().openSession();
        final @NotNull IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
        try {
            userRepository.merge(user);
            sqlSession.commit();
        } catch (SQLException e) {
            sqlSession.rollback();
            result = null;
        }
        finally {
            sqlSession.close();
        }
        return result;
    }

    @Override
    public void remove(@Nullable String id) throws SQLException {
        if (id == null || id.isEmpty()) return;
        if (bootstrap.getSqlSessionFactory() == null) return;
        final @NotNull SqlSession sqlSession = bootstrap.getSqlSessionFactory().openSession();
        final @NotNull IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
        try {
            userRepository.removeOne(id);
            sqlSession.commit();
        } catch (SQLException e) {
            sqlSession.rollback();
        }
        finally {
            sqlSession.close();
        }
    }

    @Override
    public void removeAll() throws SQLException {
        if (bootstrap.getSqlSessionFactory() == null) return;
        final @NotNull SqlSession sqlSession = bootstrap.getSqlSessionFactory().openSession();
        final @NotNull IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
        try {
            userRepository.removeAll();
            sqlSession.commit();
        } catch (SQLException e) {
            sqlSession.rollback();
        }
        finally {
            sqlSession.close();
        }
    }

    @Override
    public boolean checkLogin(@Nullable final String name) throws SQLException {
        if (name == null) return false;
        if (name.isEmpty()) return false;
        @NotNull final User user = findByLoginName(name);
        if (user == null) return false;
        if (user.getLoginName().equals(name)) return true;
        return false;
    }

}
