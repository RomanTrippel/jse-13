package ru.trippel.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trippel.tm.entity.Project;

import java.sql.SQLException;
import java.util.List;

public interface IProjectRepository {

    @Insert("INSERT INTO tm.app_project (id, name, description, date_start, date_finish, user_id, status, date_create) " +
            "VALUES (#{id}, #{name}, #{description}, #{dateStart}, #{dateFinish}, #{userId}, #{status}, #{dateCreate})")
    void persist(@NotNull Project project) throws SQLException;

    @Nullable
    @Select("SELECT * FROM tm.app_project WHERE user_id = #{userId} ORDER BY ${comparator}")
    @Results({
            @Result(property = "dateStart", column = "date_start"),
            @Result(property = "dateFinish", column = "date_finish"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "dateCreate", column = "date_create")
    })
    List<Project> findAllByComparator(@NotNull @Param("userId") String userId,
                                      @NotNull @Param("comparator") String comparator
    ) throws SQLException;

    @Nullable
    @Select("SELECT * FROM tm.app_project WHERE user_id = #{userId}")
    @Results({
            @Result(property = "dateStart", column = "date_start"),
            @Result(property = "dateFinish", column = "date_finish"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "dateCreate", column = "date_create")
    })
    List<Project> findAllById(@NotNull String userId) throws SQLException;

    @Nullable
    @Select("SELECT * FROM tm.app_project")
    @Results({
            @Result(property = "dateStart", column = "date_start"),
            @Result(property = "dateFinish", column = "date_finish"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "dateCreate", column = "date_create")
    })
    List<Project> findAll() throws SQLException;

    @Nullable
    @Select("SELECT * FROM tm.app_project WHERE id = #{id}")
    @Results({
            @Result(property = "dateStart", column = "date_start"),
            @Result(property = "dateFinish", column = "date_finish"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "dateCreate", column = "date_create")
    })
    Project findOne(@NotNull String id) throws SQLException;

    @Nullable
    @Select("SELECT * FROM tm.app_project WHERE user_id = #{userId} " +
            "AND (name LIKE #{searchText} OR description LIKE #{searchText})")
    @Results({
            @Result(property = "dateStart", column = "date_start"),
            @Result(property = "dateFinish", column = "date_finish"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "dateCreate", column = "date_create")
    })
    List<Project> findByPart(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("searchText") String searchText
    ) throws SQLException;

    @Delete("DELETE FROM tm.app_project WHERE user_id = #{userId}")
    void clear(@NotNull String userId) throws SQLException;

    @Delete("DELETE FROM tm.app_project")
    void removeAll() throws SQLException;

    @Delete("DELETE FROM tm.app_project WHERE id = #{id}")
    void removeOne(@NotNull String id) throws SQLException;

    @Update("UPDATE tm.app_project SET name = #{name} , description = #{description}, date_start = #{dateStart}, " +
            "date_finish = #{dateFinish}, status = #{status}, date_create = #{dateCreate} " +
            "WHERE user_id = #{userId} AND id = #{id}")
    void merge(@NotNull Project project) throws SQLException;

}
