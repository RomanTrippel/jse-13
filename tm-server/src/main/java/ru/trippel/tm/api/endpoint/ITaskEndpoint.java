package ru.trippel.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trippel.tm.entity.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface ITaskEndpoint {

    @Nullable
    @WebMethod
    Task createTask(
            @WebParam(name = "token") @NotNull String token,
            @WebParam(name = "taskName") @NotNull String taskName
    ) throws Exception;

    @WebMethod
    void clearTasks(
            @WebParam(name = "token") @NotNull String token
    ) throws Exception;

    @WebMethod
    void removeAllTasks(
            @WebParam(name = "token") @NotNull String token
    ) throws Exception;

    @WebMethod
    List<Task> findAllTask(
            @WebParam(name = "token") @NotNull String token
    ) throws Exception;

    @Nullable
    @WebMethod
    Task findOneTask(
            @WebParam(name = "token") @NotNull String token,
            @WebParam(name = "taskId") @NotNull String taskId
    ) throws Exception;

    @Nullable
    @WebMethod
    Task updateTask(
            @WebParam(name = "token") @NotNull String token,
            @WebParam(name = "task") @NotNull Task task
    ) throws Exception;

    @Nullable
    @WebMethod
    void removeTask(
            @WebParam(name = "token") @NotNull String token,
            @WebParam(name = "taskId") @NotNull String taskId
    ) throws Exception;

    @Nullable
    @WebMethod
    List<Task> findAllTasksBySort(
            @WebParam(name = "token") @NotNull String token
    ) throws Exception;

    @Nullable
    @WebMethod
    List<Task> findAllTasksByUserId(
            @WebParam(name = "token") @NotNull String token
    ) throws Exception;

    @WebMethod
    void removeAllTaskByProjectId(
            @WebParam(name = "token") @NotNull String token,
            @WebParam(name = "projectId") @NotNull String projectId
    ) throws Exception;

    @Nullable
    @WebMethod
    List<Task> findTaskByPart(
            @WebParam(name = "token") @NotNull String token,
            @WebParam(name = "searchPhrase") @NotNull String searchText
    ) throws Exception;

}
