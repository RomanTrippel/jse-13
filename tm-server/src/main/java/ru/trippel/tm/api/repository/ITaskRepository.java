package ru.trippel.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trippel.tm.entity.Task;

import java.sql.SQLException;
import java.util.List;

public interface ITaskRepository {

    @Insert("INSERT INTO tm.app_task (id, name, description, date_start, date_finish, " +
            "project_id, user_id, status, date_create) " +
            "VALUES (#{id}, #{name}, #{description}, #{dateStart}, #{dateFinish}, " +
            "#{projectId}, #{userId}, #{status}, #{dateCreate})")
    void persist(@NotNull Task task) throws SQLException;

    @Nullable
    @Select("SELECT * FROM tm.app_task WHERE user_id = #{userId} ORDER BY ${comparator}")
    @Results({
            @Result(property = "dateStart", column = "date_start"),
            @Result(property = "dateFinish", column = "date_finish"),
            @Result(property = "projectId", column = "project_id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "dateCreate", column = "date_create")
    })
    List<Task> findAllByComparator(@NotNull @Param("userId") String userId,
                                   @NotNull @Param("comparator") String comparator
    ) throws SQLException;

    @Nullable
    @Select("SELECT * FROM tm.app_task WHERE user_id = #{userId}")
    @Results({
            @Result(property = "dateStart", column = "date_start"),
            @Result(property = "dateFinish", column = "date_finish"),
            @Result(property = "projectId", column = "project_id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "dateCreate", column = "date_create")
    })
    List<Task> findAllById(@NotNull String userId) throws SQLException;

    @Nullable
    @Select("SELECT * FROM tm.app_task")
    @Results({
            @Result(property = "dateStart", column = "date_start"),
            @Result(property = "dateFinish", column = "date_finish"),
            @Result(property = "projectId", column = "project_id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "dateCreate", column = "date_create")
    })
    List<Task> findAll() throws SQLException;

    @Nullable
    @Select("SELECT * FROM tm.app_task WHERE id = #{id}")
    @Results({
            @Result(property = "dateStart", column = "date_start"),
            @Result(property = "dateFinish", column = "date_finish"),
            @Result(property = "projectId", column = "project_id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "dateCreate", column = "date_create")
    })
    Task findOne(@NotNull String id) throws SQLException;

    @Nullable
    @Select("SELECT * FROM tm.app_task WHERE user_id = #{userId} " +
            "AND (name LIKE #{searchText} OR description LIKE #{searchText})")
    @Results({
            @Result(property = "dateStart", column = "date_start"),
            @Result(property = "dateFinish", column = "date_finish"),
            @Result(property = "projectId", column = "project_id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "dateCreate", column = "date_create")
    })
    List<Task> findByPart(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("searchText") String searchText
    ) throws SQLException;

    @Delete("DELETE FROM tm.app_task WHERE user_id = #{userId}")
    void clear(@NotNull String userId) throws SQLException;

    @Delete("DELETE FROM tm.app_task WHERE id = #{id}")
    void removeOne(@NotNull String id) throws SQLException;

    @Delete("DELETE FROM tm.app_task")
    void removeAll() throws SQLException;

    @Delete("DELETE FROM tm.app_task WHERE project_id = #{projectId}")
    void removeByProjectId(@NotNull String projectId) throws SQLException;

    @Update("UPDATE tm.app_task SET name = #{name} , description = #{description}, date_start = #{dateStart}, " +
            "date_finish = #{dateFinish}, status = #{status}, date_create = #{dateCreate} " +
            "WHERE user_id = #{userId} AND project_id = #{projectId} AND id = #{id}")
    void merge(@NotNull Task task) throws SQLException;

}
