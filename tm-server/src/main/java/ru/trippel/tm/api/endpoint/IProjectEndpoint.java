package ru.trippel.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trippel.tm.entity.Project;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface IProjectEndpoint {

    @Nullable
    @WebMethod
    Project createProject(
            @WebParam(name = "token") @NotNull String token,
            @WebParam(name = "projectName") @NotNull String projectName
    ) throws Exception;

    @WebMethod
    void clearProjects(
            @WebParam(name = "token") @NotNull String token
    ) throws Exception;

    @WebMethod
    void removeAllProjects(
            @WebParam(name = "token") @NotNull String token
    ) throws Exception;

    @NotNull
    @WebMethod
    List<Project> findAllProject(
            @WebParam(name = "token") @NotNull String token
    ) throws Exception;

    @Nullable
    @WebMethod
    Project findOneProject(
            @WebParam(name = "token") @NotNull String token,
            @WebParam(name = "projectId") @NotNull String projectId
    ) throws Exception;

    @Nullable
    @WebMethod
    Project updateProject(
            @WebParam(name = "token") @NotNull String token,
            @WebParam(name = "project") @NotNull Project project
    ) throws Exception;

    @Nullable
    @WebMethod
    void removeProject(
            @WebParam(name = "token") @NotNull String token,
            @WebParam(name = "projectId") @NotNull String projectId
    ) throws Exception;

    @Nullable
    @WebMethod
    List<Project> findAllProjectsBySort(
            @WebParam(name = "token") @NotNull String token
    ) throws Exception;

    @Nullable
    @WebMethod
    List<Project> findAllProjectsByUserId(
            @WebParam(name = "token") @NotNull String token
    ) throws Exception;

    @Nullable
    @WebMethod
    List<Project> findProjectByPart(
            @WebParam(name = "token") @NotNull String token,
            @WebParam(name = "searchPhrase") @NotNull String searchText
    ) throws Exception;

}
