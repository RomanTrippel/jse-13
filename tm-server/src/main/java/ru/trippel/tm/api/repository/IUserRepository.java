package ru.trippel.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trippel.tm.entity.User;

import java.sql.SQLException;
import java.util.List;

public interface IUserRepository {

    @Insert("INSERT INTO tm.app_user (id, name, password, role, project_sort, task_sort) " +
            "VALUES (#{id}, #{loginName}, #{password}, #{role}, #{projectSortingMethod}, #{taskSortingMethod})")
    void persist(@NotNull User user) throws SQLException;

    @Nullable
    @Select("SELECT * FROM tm.app_user")
    @Results({
            @Result(property = "loginName", column = "name"),
            @Result(property = "dateFinish", column = "date_finish"),
            @Result(property = "projectSortingMethod", column = "project_sort"),
            @Result(property = "taskSortingMethod", column = "task_sort"),
    })
    List<User> findAll() throws SQLException;

    @Nullable
    @Select("SELECT * FROM tm.app_user WHERE id = #{id}")
    @Results({
            @Result(property = "loginName", column = "name"),
            @Result(property = "dateFinish", column = "date_finish"),
            @Result(property = "projectSortingMethod", column = "project_sort"),
            @Result(property = "taskSortingMethod", column = "task_sort"),
    })
    User findOne(@NotNull String id) throws SQLException;

    @Nullable
    @Select("SELECT * FROM tm.app_user WHERE name = #{login}")
    @Results({
            @Result(property = "loginName", column = "name"),
            @Result(property = "dateFinish", column = "date_finish"),
            @Result(property = "projectSortingMethod", column = "project_sort"),
            @Result(property = "taskSortingMethod", column = "task_sort"),
    })
    User findByLoginName(String login) throws SQLException;

    @Update("UPDATE tm.app_user SET name = #{loginName} , password = #{password}, role = #{role}, " +
            "project_sort = #{projectSortingMethod}, task_sort = #{taskSortingMethod} WHERE id = #{id}")
    void merge(@NotNull User user) throws SQLException;

    @Delete("DELETE FROM tm.app_user WHERE id = #{id}")
    void removeOne(@NotNull String id) throws SQLException;

    @Delete("DELETE FROM tm.app_user")
    void removeAll() throws SQLException;

}
