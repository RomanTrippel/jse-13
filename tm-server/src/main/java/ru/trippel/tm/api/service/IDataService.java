package ru.trippel.tm.api.service;

import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.sql.SQLException;

public interface IDataService {

    void dataSerializationSave() throws IOException, SQLException;

    void dataFasterxmlXmlSave() throws IOException, SQLException;

    void dataFasterxmlJsonSave() throws IOException, SQLException;

    void dataJaxbXmlSave() throws JAXBException, SQLException;

    void dataJaxbJsonSave() throws JAXBException, SQLException;

    void dataSerializationLoad() throws IOException, ClassNotFoundException, SQLException;

    void dataFasterxmlXmlLoad() throws IOException, SQLException;

    void dataFasterxmlJsonLoad() throws IOException, SQLException;

    void dataJaxbXmlLoad() throws JAXBException, SQLException;

    void dataJaxbJsonLoad() throws JAXBException, SQLException;

}
