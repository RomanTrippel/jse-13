package ru.trippel.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import ru.trippel.tm.entity.Session;

public interface ISessionRepository {

    @Insert("INSERT INTO tm.app_session (id, user_id, signature, create_date, role) " +
            "VALUES (#{id}, #{userId}, #{signature}, #{createDate}, #{role})")
    void persist(@NotNull Session session);

    @Delete("DELETE FROM app_session WHERE id = #{id}")
    void remove(@NotNull final String id);

    @Select("SELECT * FROM tm.app_session WHERE id = #{sessionId}")
    @Results({
    @Result(property = "userId", column = "user_id"),
    @Result(property = "createDate", column = "create_date")
    })
    Session findOne(@NotNull String sessionId);

}
