package ru.trippel.tm.util;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.trippel.tm.enumeration.SortingMethod;

@NoArgsConstructor
public final class ComparatorUtil {

    @NotNull
    public static String getComparator(@NotNull SortingMethod sortingMethod){
        switch (sortingMethod) {
            case CREATION_ORDER:
                return "date_create";
            case START_DATE:
                return "date_start";
            case FINISH_DATE:
                return "date_finish";
            case STATUS:
                return "status";
        }
        return "date_create";
    }

}
