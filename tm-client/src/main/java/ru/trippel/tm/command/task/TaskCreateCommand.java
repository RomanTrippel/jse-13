package ru.trippel.tm.command.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trippel.tm.api.endpoint.Session;
import ru.trippel.tm.command.AbstractCommand;

@NoArgsConstructor
public final class TaskCreateCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getNameCommand() {
        return "task create";
    }

    @Override
    public boolean secure() {
        return false;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Create a new task.";
    }

    @Override
    public void execute() throws Exception {
        @Nullable final String token = serviceLocator.getStateService().getToken();
        System.out.println("Enter task name.");
        @NotNull final String name = serviceLocator.getTerminalService().read();
        if (name.isEmpty()) {
            System.out.println("An empty name is entered. Try it again.");
        }
        else {
            serviceLocator.getTaskEndpoint().createTask(token, name);
            System.out.println("The task \"" + name + "\" added!");
        }
    }

}
