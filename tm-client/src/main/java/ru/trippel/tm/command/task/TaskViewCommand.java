package ru.trippel.tm.command.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trippel.tm.api.endpoint.Exception_Exception;
import ru.trippel.tm.api.endpoint.Task;
import ru.trippel.tm.api.endpoint.Session;
import ru.trippel.tm.command.AbstractCommand;
import ru.trippel.tm.util.TaskPrintUtil;

import java.util.List;

@NoArgsConstructor
public final class TaskViewCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getNameCommand() {
        return "task view";
    }

    @Override
    public boolean secure() {
        return false;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show all tasks.";
    }

    @Override
    public void execute() throws Exception_Exception {
        @Nullable final String token = serviceLocator.getStateService().getToken();
        if (token == null) return;
        @Nullable final List<Task> TaskList = serviceLocator.getTaskEndpoint().findAllTasksBySort(token);
        if (TaskList == null || TaskList.isEmpty()) {
            System.out.println("List is empty.");
            return;
        }
        System.out.println("Task List:");
        for (int i = 0; i < TaskList.size(); i++) {
            Task Task = TaskList.get(i);
            System.out.println(i+1 + ". " + TaskPrintUtil.print(Task));
        }
    }

}
