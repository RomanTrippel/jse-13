package ru.trippel.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import ru.trippel.tm.api.endpoint.*;

import java.lang.Exception;

public class ProjectEndpointServiceTest {

    @NotNull
    private static final ISessionEndpoint sessionEndpoint = new SessionEndpointService().getSessionEndpointPort();

    @NotNull
    private static final IUserEndpoint userEndpoint = new UserEndpointService().getUserEndpointPort();

    @NotNull
    private static final IProjectEndpoint projectEndpoint = new ProjectEndpointService().getProjectEndpointPort();

    @Nullable
    private static User userTest;

    @Nullable
    private static String tokenUser;

    @Nullable
    private static String tokenAdmin;

    @BeforeClass
    public static void addUser() throws Exception {
        userTest = userEndpoint.createUser("test", "test");
        tokenUser = sessionEndpoint.createSession("user", "user");
        tokenAdmin = sessionEndpoint.createSession("admin", "admin");
    }

    @Test
    public void createProject() throws Exception_Exception {
        projectEndpoint.createProject(tokenUser, "TestProject");
        @Nullable final String projectName = projectEndpoint.findAllProjectsByUserId(tokenUser).get(0).getName();
        @Nullable final String projectId = projectEndpoint.findAllProject(tokenUser).get(0).getId();
        Assert.assertEquals("TestProject", projectName);
        projectEndpoint.removeProject(tokenUser, projectId);

    }

    @Test
    public void removeProject() throws Exception_Exception {
        projectEndpoint.createProject(tokenUser, "TestProject");
        @Nullable final String projectId = projectEndpoint.findAllProject(tokenUser).get(0).getId();
        projectEndpoint.removeProject(tokenUser, projectId);
        @Nullable final Project project = projectEndpoint.findOneProject(tokenUser, projectId);
        Assert.assertEquals(null, project);
    }

    @Test
    public void clearProject() throws Exception_Exception {
        projectEndpoint.createProject(tokenUser, "TestProject");
        projectEndpoint.createProject(tokenUser, "TestProject1");
        projectEndpoint.clearProjects(tokenUser);
        @Nullable final int size = projectEndpoint.findAllProject(tokenUser).size();
        Assert.assertNotEquals(null, size);
    }

    @Test
    public void removeAllProject() throws Exception_Exception {
        projectEndpoint.createProject(tokenUser, "TestProject");
        projectEndpoint.createProject(tokenUser, "TestProject1");
        projectEndpoint.createProject(tokenAdmin, "TestProjectAdmins");
        projectEndpoint.removeAllProjects(tokenAdmin);
        @Nullable final int size = projectEndpoint.findAllProject(tokenAdmin).size();
        Assert.assertNotEquals(null, size);
    }

    @Test
    public void findAllProject() throws Exception_Exception {
        projectEndpoint.createProject(tokenUser, "TestProject");
        projectEndpoint.createProject(tokenUser, "TestProject1");
        @Nullable final int size = projectEndpoint.findAllProject(tokenAdmin).size();
        Assert.assertEquals(2, size);
        projectEndpoint.removeAllProjects(tokenAdmin);
    }

    @Test
    public void findOneProject() throws Exception_Exception {
        projectEndpoint.createProject(tokenUser, "TestProject");
        @Nullable final String projectId = projectEndpoint.findAllProject(tokenUser).get(0).getId();
        @Nullable final Project project = projectEndpoint.findOneProject(tokenUser, projectId);
        Assert.assertEquals("TestProject", project.getName());
        projectEndpoint.removeAllProjects(tokenAdmin);
    }

    @Test
    public void updateProject() throws Exception_Exception {
        projectEndpoint.createProject(tokenUser, "TestProject");
        @Nullable final Project project = projectEndpoint.findAllProjectsByUserId(tokenUser).get(0);
        @NotNull final String projectId = project.getId();
        project.setDescription("new");
        projectEndpoint.updateProject(tokenUser, project);
        @Nullable final Project project1 = projectEndpoint.findOneProject(tokenUser, projectId);
        Assert.assertEquals("new", project1.getDescription());
        projectEndpoint.clearProjects(tokenUser);
    }

    @Test
    public void findAllProjectByUserId() throws Exception_Exception {
        projectEndpoint.createProject(tokenUser, "TestProject");
        projectEndpoint.createProject(tokenUser, "TestProject1");
        @Nullable final int size = projectEndpoint.findAllProjectsByUserId(tokenUser).size();
        Assert.assertEquals(2, size);
        projectEndpoint.clearProjects(tokenUser);
    }

    @Test
    public void findProjectByPart() throws Exception_Exception {
        projectEndpoint.createProject(tokenUser, "TestProject");
        projectEndpoint.createProject(tokenUser, "TestProject1");
        @Nullable final String projectName =
                projectEndpoint.findProjectByPart(tokenUser, "TestProject").get(0).getName();
        @Nullable final String projectName1 =
                projectEndpoint.findProjectByPart(tokenUser, "TestProject1").get(0).getName();
        Assert.assertEquals("TestProject", projectName);
        Assert.assertEquals("TestProject1", projectName1);
        projectEndpoint.clearProjects(tokenUser);
    }

    @AfterClass
    public static void removeUser() throws Exception {
        userEndpoint.removeUser(tokenUser, userTest.getId());
        sessionEndpoint.removeSession(tokenUser);
        sessionEndpoint.removeSession(tokenAdmin);
    }

}
