package ru.trippel.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import ru.trippel.tm.api.endpoint.*;

import java.lang.Exception;

public class SessionEndpointServiceTest {

    @NotNull
    private static final ISessionEndpoint sessionEndpoint = new SessionEndpointService().getSessionEndpointPort();

    @NotNull
    private static final IUserEndpoint userEndpoint = new UserEndpointService().getUserEndpointPort();

    @NotNull
    private static final IProjectEndpoint projectEndpoint = new ProjectEndpointService().getProjectEndpointPort();

    @Nullable
    private static User userTest;

    @Nullable
    private static String tokenUser;

    @Nullable
    private static String tokenRemove;

    {
        try {
            tokenRemove = sessionEndpoint.createSession("test", "test");
        } catch (Exception_Exception e) {
            e.printStackTrace();
        }
    }

    @BeforeClass
    public static void addUser() throws Exception {
        userTest = userEndpoint.createUser("test", "test");
    }

    @Test
    public void createSessionWithPassword() throws Exception_Exception {
        tokenUser = sessionEndpoint.createSession("test", "test");
        Assert.assertNotEquals(null, userTest);
    }

    @Test(expected = Exception.class)
    public void createSessionWithoutPassword() throws Exception_Exception {
        tokenUser = sessionEndpoint.createSession("test", "");
    }

    @Test(expected = Exception.class)
    public void createSessionWithWrongPassword() throws Exception_Exception {
        tokenUser = sessionEndpoint.createSession("test", "test1");
    }

    @Test(expected = Exception_Exception.class)
    public void removeSession() throws Exception_Exception {
        @NotNull String tokenUser = sessionEndpoint.createSession("test", "test");
        sessionEndpoint.removeSession(tokenUser);
        projectEndpoint.findAllProjectsByUserId(tokenUser);
    }

    @After
    public void removeAllSession() throws Exception_Exception {
        sessionEndpoint.removeSession(tokenUser);
    }

    @AfterClass
    public static void removeUser() throws Exception {
        userEndpoint.removeUser(tokenRemove, userTest.getId());
    }

}
