package ru.trippel.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import ru.trippel.tm.api.endpoint.*;

import java.lang.Exception;

public class TaskEndpointServiceTest {

    @NotNull
    private static final ISessionEndpoint sessionEndpoint = new SessionEndpointService().getSessionEndpointPort();

    @NotNull
    private static final IUserEndpoint userEndpoint = new UserEndpointService().getUserEndpointPort();

    @NotNull
    private static final ITaskEndpoint taskEndpoint = new TaskEndpointService().getTaskEndpointPort();

    @Nullable
    private static User userTest;

    @Nullable
    private static String tokenUser;

    @Nullable
    private static String tokenAdmin;

    @BeforeClass
    public static void addUser() throws Exception {
        userTest = userEndpoint.createUser("test", "test");
        tokenUser = sessionEndpoint.createSession("user", "user");
        tokenAdmin = sessionEndpoint.createSession("admin", "admin");
    }

    @Test
    public void createTask() throws Exception_Exception {
        taskEndpoint.createTask(tokenUser, "TestTask");
        @Nullable final String taskName = taskEndpoint.findAllTasksByUserId(tokenUser).get(0).getName();
        @Nullable final String taskId = taskEndpoint.findAllTask(tokenUser).get(0).getId();
        Assert.assertEquals("TestTask", taskName);
        taskEndpoint.removeTask(tokenUser, taskId);

    }

    @Test
    public void removeTask() throws Exception_Exception {
        taskEndpoint.createTask(tokenUser, "TestTask");
        @Nullable final String taskId = taskEndpoint.findAllTask(tokenUser).get(0).getId();
        taskEndpoint.removeTask(tokenUser, taskId);
        @Nullable final Task task = taskEndpoint.findOneTask(tokenUser, taskId);
        Assert.assertEquals(null, task);
    }

    @Test
    public void clearTask() throws Exception_Exception {
        taskEndpoint.createTask(tokenUser, "TestTask");
        taskEndpoint.createTask(tokenUser, "TestTask1");
        taskEndpoint.clearTasks(tokenUser);
        @Nullable final int size = taskEndpoint.findAllTask(tokenUser).size();
        Assert.assertNotEquals(null, size);
    }

    @Test
    public void removeAllTask() throws Exception_Exception {
        taskEndpoint.createTask(tokenUser, "TestTask");
        taskEndpoint.createTask(tokenUser, "TestTask1");
        taskEndpoint.createTask(tokenAdmin, "TestTaskAdmins");
        taskEndpoint.removeAllTasks(tokenAdmin);
        @Nullable final int size = taskEndpoint.findAllTask(tokenAdmin).size();
        Assert.assertNotEquals(null, size);
    }

    @Test
    public void findAllTask() throws Exception_Exception {
        taskEndpoint.createTask(tokenUser, "TestTask");
        taskEndpoint.createTask(tokenUser, "TestTask1");
        @Nullable final int size = taskEndpoint.findAllTask(tokenAdmin).size();
        Assert.assertEquals(2, size);
        taskEndpoint.removeAllTasks(tokenAdmin);
    }

    @Test
    public void findOneTask() throws Exception_Exception {
        taskEndpoint.createTask(tokenUser, "TestTask");
        @Nullable final String taskId = taskEndpoint.findAllTask(tokenUser).get(0).getId();
        @Nullable final Task task = taskEndpoint.findOneTask(tokenUser, taskId);
        Assert.assertEquals("TestTask", task.getName());
        taskEndpoint.removeAllTasks(tokenAdmin);
    }

    @Test
    public void updateTask() throws Exception_Exception {
        taskEndpoint.createTask(tokenUser, "TestTask");
        @Nullable final Task task = taskEndpoint.findAllTasksByUserId(tokenUser).get(0);
        @NotNull final String taskId = task.getId();
        task.setDescription("new");
        taskEndpoint.updateTask(tokenUser, task);
        @Nullable final Task task1 = taskEndpoint.findOneTask(tokenUser, taskId);
        Assert.assertEquals("new", task1.getDescription());
        taskEndpoint.clearTasks(tokenUser);
    }

    @Test
    public void findAllTaskByUserId() throws Exception_Exception {
        taskEndpoint.createTask(tokenUser, "TestTask");
        taskEndpoint.createTask(tokenUser, "TestTask1");
        @Nullable final int size = taskEndpoint.findAllTasksByUserId(tokenUser).size();
        Assert.assertEquals(2, size);
        taskEndpoint.clearTasks(tokenUser);
    }

    @Test
    public void findTaskByPart() throws Exception_Exception {
        taskEndpoint.createTask(tokenUser, "TestTask");
        taskEndpoint.createTask(tokenUser, "TestTask1");
        @Nullable final String taskName =
                taskEndpoint.findTaskByPart(tokenUser, "TestTask").get(0).getName();
        @Nullable final String taskName1 =
                taskEndpoint.findTaskByPart(tokenUser, "TestTask1").get(0).getName();
        Assert.assertEquals("TestTask", taskName);
        Assert.assertEquals("TestTask1", taskName1);
        taskEndpoint.clearTasks(tokenUser);
    }

    @AfterClass
    public static void removeUser() throws Exception {
        userEndpoint.removeUser(tokenUser, userTest.getId());
        sessionEndpoint.removeSession(tokenUser);
        sessionEndpoint.removeSession(tokenAdmin);
    }

}
